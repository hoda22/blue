<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Requests\brand\BrandStoreRequest;
use App\Http\Requests\brand\BrandUpdateRequest;
use App\Http\Resources\BrandResource;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::get();
        if ( count($brands) )
            return BrandResource::collection($brands)->additional(["status"=>true]);
        else
            return response()->json(["status"=>false]);
    }

    public function show($id){
        $brand = Brand::findOrFail($id);
        return ( new BrandResource($brand) )->additional(['status'=>true]);
    }

    public function store(BrandStoreRequest $request)
    {
        Brand::create($request->validated());
        return response()->json(["status"=>true , "message"=>__("تم الاضافه بنجاح")]);
    }

    public function delete( $id )
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return response()->json(["status"=>true , "message"=>__("تم المسح بنجاح")]);
    }

    public function update(BrandUpdateRequest $request , $id){
        Brand::findOrFail($id)->update($request->validated());
        return response()->json(["status"=>true , "message"=>__("تم التعديل بنجاح")]);
    }
}
