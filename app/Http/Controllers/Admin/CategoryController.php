<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\category\CategoryStoreRequest;
use App\Http\Requests\category\CategoryUpdateRequest;
use App\Http\Resources\CategoryResource;
class CategoryController extends Controller
{
    public function index(){
         $categories = Category::get();
         if ( count($categories) )
            return CategoryResource::collection($categories)->additional(["status"=>true]);
         else
            return response()->json(["status"=>false]);

    }

    public function store(CategoryStoreRequest $request)
    {
        Category::create($request->validated());
        return response()->json(["status"=>true]);
    }

    public function show($id){
        $category = Category::findOrFail($id);
        return ( new CategoryResource($category) )->additional(['status'=>true]);
    }

    public function delete( $id )
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return response()->json(["status"=>true]);
    }

    public function update( CategoryUpdateRequest $request , $id){
        Category::findOrFail($id)->update($request->validated());
        return response()->json(["status"=>true , "message"=>__("تم التعديل بنجاح")]);
    }

}
