<?php

namespace App\Http\Controllers;

use App\Http\Requests\brand\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $products = Product::with("brandCategory")->get();
        if ( count($products) )
            return ProductResource::collection($products)->additional(["status"=>true]);
        else
            return response()->json(["status"=>false]);
    }

    public function store(Request $request)
    {
        Product::create($request->validated());
        return response()->json(["status"=>true]);
    }

    public function delete( $id )
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json(["status"=>true]);
    }

    public function update( ProductUpdateRequest $request , $id){
        $category = Product::findOrFail($id)->update($request->validated());
        return response()->json(["status"=>true , "message"=>__("تم التعديل بنجاح")]);
    }
}
