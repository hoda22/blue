<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use Response;
use Auth;

use Illuminate\Http\Request;

class AuthController extends Controller
{


    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:patients',
            'password' => 'required|confirmed|min:6',
        ];

        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
            ];
            return response()->json($response);
        }

        $userToken = str_random(60);

        $request->merge(array('api_token' => $userToken));
        $request->merge(['password' => Hash::make($request->password)]);
        $user = User::create($request->except("password_confirmation"));

        if ($user) {
            $data = [
                'status' => true,
                'message' => trans(' تم النسجيل بنجاح'),
                'data' => $user
            ];
            return response()->json($data, 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => [trans('front.error_try_again')],
            ], 200);
        }
    }

    public function login(Request $request)
    {
//        return $request->all();
        $validation = validator()->make($request->all(), [
            'email' => 'required',
            'password' => 'required|min:3'
        ]);

        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
            ];
            return response()->json($response);
        }

        $user = User::where('email', $request->email)->first();



        if ($user) {

            if (Hash::check($request->password, $user->password)) {
                // if successful, then redirect to their intended location
                if (in_array( $user->status , ['rejected', 'wait'])) {
                    $user->update(['api_token' => null]);
                    return response()->json([
                        'status' => false,
                        'logout' => true,
                        'error' => [__('لم يقبل بوسطه الاداره ')]
                    ]);
                }
                $user->api_token = $this->generateRandomString(60);

                $user->save();

                return response()->json(['status' => true, 'data' => $user]);
            } else {
                $response = [
                    'status' => false,
                    'message' => 'البريد الالكترونى أو كلمة المرور غير صحيحة',
                ];
                return response()->json($response, 200);
            }
        }

        $response = [
            'status' => false,
            'message' => 'لا يوجد حساب مطابق',
        ];
        return response()->json($response, 200);
    }

    public function forgetPassword(Request $request)
    {
        $client = User::where('email', $request->email)->orWhere('phone', $request->email)->first();
        if ($client) {
            $forget_code = random_int(10000000, 99999999);
            $client->forget_code = $forget_code;
            $client->save();
            $email = $client->email;
            $data = array(
                'active_code' => $forget_code,
                'name' => $client->name,
            );
//            Mail::send('emails.mail', ['data' => $data], function ($message) use ($email) {
//                $message->to($email)
//                    ->subject('Salon Tech Forget Code');
//            });

            return response()->json([
                'status' => true,
                'message' => trans('تم ارسال الكود علي الايميل')
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => [trans('المعلومات غير مسجلة')]
            ]);
        }
    }

    public function newPassword(Request $request)
    {
        $messages = [
            'email.required' => "ايميل العميل مطلوب",
            'code.required' => "الكود مطلوب",

        ];
        $validator = validator()->make($request->all(), [

            'password' => 'required',
            'code' => 'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data
            ];
            return response()->json($response);
        }
        $row = Patient::where('forget_code', $request->forget_code)->first();
        if (is_null($row)) {
            return response()->json([
                'status' => false,
                'message' => trans('الكود غير صحيح')
            ]);
        }

        $row->password = bcrypt($request->password);
        $row->save();
        return response()->json([
            'status' => true,
            'message' => trans('تم تغيير كلمه المرور'),

        ]);
    }

    public function resetPassword(Request $request)
    {
        $validation = validator()->make($request->all(), [
            'email' => 'required|email',
            'code' => 'required',
            'password' => 'required',
        ]);
        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
            ];
            return response()->json($response);
        }
        $user = Patient::where('email', $request->email)->where('forget_code', $request->code)->first();
        if ($user) {
            $password = $request->password;
            $user->password = bcrypt($password);
            $user->forget_code = null;
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'تم تغيير كلمة المرور'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'تأكد من الكود والبريد الالكتروني'
            ], 200);
        }
    }

    public function updateProfile(Request $request)
    {
        $id = auth()->guard('patients')->user()->id;
        $messages = [
            'name.required' => 'الاسم مطلوب',
            'user_name.required' => 'اسم المستخدم مطلوب',
            'email.required' => 'البريد الالكتروني مطلوب',
            'phone.required' => 'الجوال مطلوب',
            'phone.unique' => 'الجوال مستخدم من قبل',
            'email.unique' => 'البريد الالكتروني مستخدم من قبل',
            'lon.required' => 'برجاء تحديد موقك علي الخريطة',
            'lat.required' => 'برجاء تحديد موقك علي الخريطة',
            'city_id.required' => 'المدينة مطلوبة',
            'password.required' => 'كلمة المرور مطلوبة',
            'password.confirmed' => 'كلمة المرور غير متطابقة مع حقل التأكيد',
        ];
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:doctors,email,' . $id,
            'phone' => 'required|unique:doctors,phone,' . $id,

            'image' => 'image|mimes:jpg,jpeg,bmp,png',
            'player_id' => 'required',
        ];
        $validation = validator()->make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
// 'message' => 'من فضلك أدخل جميع الحقول وتأكد من صحة رقم الهاتف',
            ];
            return response()->json($response);
        }


        $client = auth()->guard('patients')->user();

        $client->update($request->all());
        if ($client) {
            return  response()->json([ 'status' => true, "data"=>$client ]);
        }

    }


    public function changePassword(Request $request)
    {
        $rules = [
            'password' => 'required',
            'current_password' => 'required',
        ];

        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            $errors = $validation->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data,
            ];
            return response()->json($response);
        }
        $user = auth()->guard('patients')->user();


        if (Hash::check($request->current_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'تم تغيير كلمة المرور'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'كلمة المرور خاطئة'
            ]);
        }
    }

    public function activeAccount(Request $request)
    {
        $messages = [
            'email.required' => "ايميل العميل مطلوب",
            'code.required' => "الكود مطلوب",
        ];
        $validator = validator()->make($request->all(), [
            'email' => 'required',
            'code' => 'required'
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $error_data = [];
            foreach ($errors->all() as $error) {
                array_push($error_data, $error);
            }
            $data = $error_data;
            $response = [
                'status' => false,
                'error' => $data
            ];
            return response()->json($response);
        }
        $row = Client::where('active_code', $request->code)->where(function ($q) use ($request){
            $q->where('email', $request->email)->orWhere('phone', $request->email);
        })->first();
        if (is_null($row)) {
            return response()->json([
                'status' => false,
                'message' => 'كود غير صحيح'
            ]);
        }
        $row->block = "unblock";
        $row->active_code = null;
        $row->save();
        return response()->json([
            'status' => true,
            'message' => 'تم تفعيل الحساب بنجاح يمكنك تسجيل الدخول الان',
        ]);
    }

    public function deleteImage($name)
    {
        $deletepath = base_path($name);
        if (file_exists($deletepath) and $name != '') {
            unlink($deletepath);
        }

        return true;
    }


    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
