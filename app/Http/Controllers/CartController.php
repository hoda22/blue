<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartDetails;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add( Request $request )
    {
        $user_id = 1;

        Cart::firstOrCreate(["user_id"=>$user_id])->details()->create(["product_id"=>$request->id]);;

        return response()->json(["status"=>true , "message"=>__("تمت الاضافه بنجاح")]) ;
    }

    public function getCart()
    {
        $user_id = 1;
        $cart = Cart::where("user_id",$user_id)->with("details.product.brandCategory")->first();
        $products = $cart->details;
        $data = [];
        if ($cart){
            foreach ($products as $product ){
                $data[] = $product->product;
            }
            return ProductResource::collection($data)->additional(["status"=>true]);
        }else{

        }
        return response()->json(["status"=>false , "message"=>__("لا يوجد منتجات في السله")]) ;

    }


    public function remove( Request $request )
    {
        $user_id = 1;
        $cart = Cart::where(["user_id"=>$user_id])
            ->first();


//        return $request->id;

        $item = CartDetails::where(["product_id" => $request->id , 'cart_id' => $cart->id])->first();
//        return $item ;
        $item->delete();
        return response()->json(["status"=>true , "message"=>__("تمت الازاله بنجاح")]) ;
    }

}
