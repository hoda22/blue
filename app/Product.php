<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=['id'];

    protected $hidden = ['created_at','updated_at'];

//    protected $appends =["brandCategory"];

    public function brandCategory(){
        return $this->belongsTo(BrandCategory::class)->with("brand","category");
    }

    public function setImageAttribute($value)
    {
        if ($value) {
            $this->deleteImage($this->image);
            $name = time() . '' . rand(11111, 99999) . '.' . $value->getClientOriginalExtension();

            $image = Image::make($value);
            $image->widen(587, null);
            $image->save('uploads/clients/' . $name);
            $this->attributes['image'] = 'uploads/clients/' . $name;

        }
    }

    public function deleteImage($name)
    {
        $deletepath = base_path($name);
        if (file_exists($deletepath) and $name != '') {
            unlink($deletepath);
        }

        return true;
    }

    public function getImage()
    {
//        return $this->image ;
        $path = base_path($this->image);
        if (file_exists($path) and $this->image != '') {
            return url($this->image);
//            return $this->image ;
        }
        return url('assets/admin/img/logo.png');

//        return 'assets/admin/img/logo.png';
    }
}
