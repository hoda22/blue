<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded=['id'];

    protected $hidden =['created_at','updated_at'];

    public function categories(){
        return $this->belongsToMany(Category::class, 'brand_category');
    }
}
