<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = ['id'] ;

    public function details(){
        return $this->hasMany(CartDetails::class ,'cart_id') ;
    }
}
