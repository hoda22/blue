<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('product/store', 'ProductController@store');
    Route::get('product/index', 'ProductController@index');
    Route::get('product/show', 'ProductController@show');

    Route::get('get-cart', 'CartController@getCart');
    Route::post('add-cart', 'CartController@add');
    Route::post('remove-cart', 'CartController@remove');




    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('forget-password', 'AuthController@forgetPassword');
    Route::post('new-password', 'AuthController@newPassword');
    Route::post('change-password-sick', 'AuthController@changePassword');


    /*
     *  Admin
     */

    Route::post('register', 'AuthController@register');

    /*
     * Brand
     */
    Route::group(['prefix' => 'brand'], function () {
        Route::post('store', 'BrandController@store');
        Route::post('delete', 'BrandController@delete');
        Route::post('show', 'BrandController@show');
        Route::post('update', 'BrandController@update');
    });
    /*
     * Category
     */
    Route::group(['prefix' => 'category'], function () {
        Route::post('store', 'CategoryController@store');
        Route::post('delete', 'CategoryController@delete');
        Route::post('show', 'CategoryController@show');
        Route::post('update', 'CategoryController@update');
    });






});